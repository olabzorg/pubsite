<div class="blog-entry inner">
  <h3 class="cover-heading">Initiation into InfoSec's Alchemical Order</h3>
  <p>
    I began my venture into security work on pretty cool invitation from a friend to go with them to DefCon.
    Vegas lights and hotels full of some of the greatest hacking talent, I found my new fam. Sets of lockpicks,
    rooted shells, and finding new script-kiddy tools to cut my teeth on became recurring obsessions. 
  </p>
  
  <p>
    Unfortunately, unlike most of the minds in the room, mine had been introduced to programming rather late.
    Many of the coding CTF's escaped me, thwarting any attempt to make progress. I sucked. I still suck. It's
    something you get used to, because it means you're getting better, pushing yourself. 
  </p>
  
  <p>
    My experience coping with initial noob-fever isn't unique, we all sucked at some point or currently at some thing.
    That's really not the point of this series of blog posts. More it's to recant my early experiences in the first 
    three or so years of working in infosec.
  </p>
  
  <p>
    So my friends give me my first taste of hackerdom, and it's a rocky on-off relationship with the work.
    Like so much else, I lost focus, became distracted with life and too much fun. My college ended up fucking me,
    got arrested for resisting arrest, and I decided to figure my life out.
  </p>
  
  <p>
    To save the boring details, I graduated, and got a kickass job working at a boutique consulting firm in Sunnyvale, CA.
    I spent a year working along some of the sharpest minds in the industry. It was a truly awe-inspiring experience, and
    an unprecedented opportunity. An opportunity I squandered, an experience I still cherish. 
  </p>
  
  <p>
    My friend worked at a sister consulting firm, and both companies eventually came under 
    the same British umbrella (along with a few other firms). This is when shit started to get weird.
  </p>
  
  <p>
    More and more, I was being pressured to work for companies requiring additional federal background checks.
    The company I had signed up with now wanted to know everything about my past.
  </p>
  
  <p>
    For anyone that's a fellow noid, that shit don't sit right. Basic employment and deep employment background
    checks provide employers all the information they need to know you're not a psycho, criminal or terrorist.
    At some point you just have to trust your employee's you know? Anyway, I'm getting off-topic again.
  </p>
  
  <p>
    The shit-storm that had now become my career mirrored itself in my personal life. My relationship with my
    father disintegrated in a grand supernova of violent emotion. My grandmother, one of the closest relationships
    in my life was dying of dementia. Life was pouring it on.
  </p>
  
  <p>
    Then I turn on self-destructo mode. Torch everything. Basically forcing my manager's hand into firing me, he does.
    Heard through the grapevine he may regret the decision, I personally couldn't give a fuck.
  </p>
  
  <p>
    Did I mention I had just met a woman I was convinced was the love of my life? 
  </p>
  
  <p>
    Yeah, so that happened. Get this, I also promised to support her while she moved out to California (where I was living atm).
    Not one week later after finalizing the plans, my job drops the ax on me.
  </p>
  
  <p>
    I only bring all this up to draw out the point that I was desperately attempting to keep work and personal
    life separate. The more I refused to confront the problems in both areas, the more I suffered. The more
    I refused to see the obvious connection between the different areas of my life, the more I suffered.
  </p>
  
  <p>
    Eventually (very quickly), I run out of money, lose my girlfriend, and become homeless. This is the
    period I am most grateful for. I would not trade the experience of homelessness for anything. It was an
    immensely humbling, intellectually enriching, and soul recovering journey.
  </p>
  
  <p>
    I spend three months wandering aimlessly, and land on the porch of a local Zen monastery. I spend the
    next three months meditating, chanting, and practicing three times a day, everyday. Time at the 
    zendo provided the inner peace and clarity I needed to start again, to get past the bullshit I was
    getting hung up on. I'm not promoting Zen, and am not a Buddhist. Everyone has their own path, and I'm
    just relating where I've walked so far.
  </p>
  
  <p>
    Eventually, the real world caught back up with me. Money became easier to come by, on a subsistence level.
    I had been relentlessly applying for more security work, but for many reasons failed to land anything.
    Then this opportunity comes up for a slight career shift, do some "analysis" work for one of the largest
    telcoms in the world (they likely own the wires you downloaded this page over).
    The analysis was to be performed on collected traffic crossing their portions of the backbone.
  </p>
  
  <p>
    Sounded innocent enough to begin with, but became abundantly clear these fucks were against the general populace.
    I could not contribute to tyranny, and sell them my soul (not to mention all my intellectual property). I quit.
  </p>
  
  <p>
    Back into the furnace I go.
  </p>
  
  <p>
    Luckily, I had made contact with a local hackerspace, and met a talented individual working remotely for another
    security consulting firm. Bring me back into the fold, return me to the sweet caress of salary.
  </p>
  
  <p>
    This job is where it gets truly bizarre. Certain government agencies employ certain high-level members of the company
    I had just signed up for. They had a problem with a person a few years back sharing a surname with a certain street in
    Columbia, MD. As I look back on it, it was naive of me not to think the association wouldn't come up. Even though I 
    never personally did work with that agency, it proved increasingly clear I couldn't isolate myself from it's influence.
  </p>
  
  <p>
    It is valuable to mention here that the federal background check I had previously refused, I now acquiesced.
    This is when I started to notice certain people being inserted into my life. Anyone who has ever been on the
    receiving end of social-engineering knows how this shit works. Context, framing and knowing your target.
    Once you have these three down pat, and a bit of charisma, the world of manipulation is open to you.
  </p>
  
  <p>
    Mostly, through reflecting on these encounters, I feel that the persons inserted into my life were looking for information.
    Corporate espionage and people wanting to make sure I wasn't going off the reservation. Valid latter concern, scumfucks prior.
  </p>
  
  <p>
    Before you think I am some crackpot paranoiac, though I'm that too, intelligence gathering and espionage are important tools
    in the corporate and state workshops. Influencing and knowing your target's mind allows you great control and power. I'm not
    saying I'm especially important either, everyone in our industry is on a rather short list. 
  </p>
  
  <p>
    If you don't think you'll be contacted once they know you can hack, think again. 
  </p>
  
  <p>
    They want you, they're hurting for talent. The smartest minds are fleeing to start-ups and small firms doing exciting work.
    Corporations, militaries, and government agencies have PR and culture issues out the ass, that make smart people run away
    screaming.
  </p>
  
  <p>
    But the money is great for people early in their career. The opportunities are amazing, and if you work it correctly, can
    be a huge benefit in your life. However, there are so many traps along the way, a few I've fell into.
  </p>
  
  <p>
    One last point I want to drive home before ending this post,<br>DONT FUCK IT UP!!! 
  </p>
  
  <p>
    DefCon's infamous Hacker Jeopardy cry rings true now and into forever. There will be many opportunities for you to break the law. 
    People may come into your life that want you to do illegal shit for them. 
  </p>
  
  <p>
    DONT DO IT!!!
  </p>
  
  <p>
    Even small, seemingly innocuous shit is just an entree into deeper work, and higher likelihood of you getting fucked
    out of years of your freedom. Or even worse, forced government labor. Remember how I said they have a recruitment problem?
    Those scenes in movies where a hacker is trapped into service by a crafty g-man isn't just show, that shit happens.
  </p>
  
  <p>
    Take anything and everything you have read on this page with a grain of Salt, in the end that's all we are.
  </p>
</div>
