{{> head}}
          <div class="inner cover">
            <nav>
            <ul class="nav inner-nav">
              <li><a href"https://www.eff.org" class="btn btn-lg btn-default btn-nav">Electronic Frontier Foundation</a></li>
              <li><a href"https://www.microcorruption.com" class="btn btn-lg btn-default btn-nav">Matasano's Microcorruption CTF</a></li>
              <li><a href"https://ctftime.org" class="btn btn-lg btn-default btn-nav">CTFTime: CTF Aggregator</a></li>
              <li><a href"http://lcamtuf.coredump.cx" class="btn btn-lg btn-default btn-nav">Lcamtuf's Homepage</a></li>
              <li><a href"http://radare.org" class="btn btn-lg btn-default btn-nav">Radare</a></li>
              <li><a href"https://freeross.org" class="btn btn-lg btn-default btn-nav">Free Ross Ulbricht</a></li>
            </ul>
            </nav>
          </div>
{{> foot}}
