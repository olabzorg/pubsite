{{> head}}
          <div class="inner cover">
            <h1 class="cover-heading">If you so desire...</h1>
            <hr>
            <nav>
            <ul class="nav inner-nav">
              <li><a href="mailto:info@onicslabs.org" class="btn btn-lg btn-default btn-nav">Email</a></li>
              <li><a href="https://twitter.com/onicslabs" class="btn btn-lg btn-default btn-nav">Twitter</a></li>
            </ul>
            </nav>
          </div>
{{> foot}}
